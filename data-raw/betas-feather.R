library(tidyverse)

lut1 <- read_table2("/usr/local/freesurfer/FreeSurferColorLUT.txt", skip = 2, comment = "#", guess_max = 10000,
                    col_names = c("aparcaseg", "roi", "R", "G", "B", "A"),
                    col_types = cols(aparcaseg = col_double(), roi = col_character(),
                                     R = col_integer(), G = col_integer(), B = col_integer(), A = col_integer() )) %>%
  select(aparcaseg, roi) %>%
  filter(aparcaseg < 1000) %>%
  separate(roi, into = c("hemisphere","roi", "type", "aux1", "aux2"), sep = "-", fill = "right") %>%
  filter(str_detect(hemisphere, "^Left$|^Right$"))

lut2 <-read_table2("/usr/local/freesurfer/FreeSurferColorLUT.txt", skip = 2, comment = "#", guess_max = 10000,
                   col_names = c("aparcaseg", "roi", "R", "G", "B", "A"),
                   col_types = cols(aparcaseg = col_double(), roi = col_character(),
                                    R = col_integer(), G = col_integer(), B = col_integer(), A = col_integer() )) %>%
  select(aparcaseg, roi) %>%
  filter(aparcaseg >= 1000) %>%
  separate(roi, into = c("type","hemisphere", "roi", "aux1", "aux2"), sep = "-", fill = "right") %>%
  mutate(hemisphere = case_when(str_detect(hemisphere, "lh") ~ "Left",
                                str_detect(hemisphere, "rh") ~ "Right",
                                TRUE ~ NA_character_))

lut <- rbind(lut1, lut2) %>%
  select(aparcaseg, hemisphere) %>%
  na.omit() %>%
  mutate(hemisphere = str_to_lower(hemisphere))

rm(lut1, lut2)


# load newer runs
high_files <- list.files(file.path("data-raw","betas"),
                         pattern = "sub.*\\.feather$",
                         full.names = TRUE)

high <- tibble(files = high_files) %>%
  mutate(data = map(files, feather::read_feather)) %>%
  mutate(flag = "high")


betas <- high %>%
  select(-files) %>%
  unnest(cols=c(data)) %>%
  separate(fname, into=c("contrast","orientation","run2","basis"), sep="_") %>%
  filter(!str_detect(orientation, "ori-intercept")) %>%
  mutate(basis = str_extract(basis,"first|zeroth")) %>%
  spread(basis, beta) %>%
  rename(beta = zeroth) %>%
  mutate(
    contrast = str_remove(contrast,"con-"),
    orientation = str_remove(orientation,"ori-"),
    orientation = as.numeric(orientation),
    run2 = str_extract(run2, "[:digit:]+"),
    run2 = as.numeric(run2)) %>%
  mutate(
    orientation = if_else(sub==0 & session==1, orientation%%180, orientation),
    orientation = 2*(orientation) - 180,
    orientation = CircStats::rad(orientation)) %>%
  select(-area) %>%
  mutate(
    voxel = as.character(voxel),
    derivs = case_when(derivs == 1.1 ~ "both",
                       derivs == 1.0 ~ "first",
                       derivs == 0.0 ~ "none"),
    correlations = case_when(correlations == "FAS" ~ "FAST",
                             correlations == "non" ~ "none",
                             correlations == "AR1" ~ "AR1"),
    contrast = case_when(contrast == "0.2" ~ "low",
                         contrast == "0.3" ~ "low",
                         contrast == "0.5" ~ "low",
                         TRUE ~ "high"),
    contrast = factor(contrast, levels = c("low","high"))) %>%
  left_join(., lut, by = "aparcaseg") %>%
  filter(!is.na(hemisphere))  # remove voxels whose hemisphere is unknown

# if(any(str_detect(betas$derivs, "first"))){
#   tmp <- betas %>%
#     filter(str_detect(derivs, "first")) %>%
#     mutate(
#       beta = sign(beta) * sqrt(beta^2 + first^2),
#       derivs = "boost")
#
#   betas <- betas %>%
#     filter(!str_detect(derivs,"first")) %>%
#     bind_rows(tmp)
# }


# fst::write_fst(betas, file.path("data-raw","betas", "betas.fst"))

betas %>%
  dplyr::filter(
    sub < 10 &
      correlations == "none" &
      derivs == "none" ) %>%
  dplyr::mutate(flag2 = flag) %>%
  dplyr::group_nest(sub, derivs, correlations, flag2) %>%
  dplyr::mutate(data = purrr::map(data, vtuner::pRF_filter, sdthresh=2)) %>%
  tidyr::unnest(cols=c(data)) %>%
  dplyr::filter(where == "middle") %>%
  rename(y = beta) %>%
  rename(ses = session) %>%
  select(sub, run2, voxel, contrast, orientation, y, ses, spmT, spmTavg, spmF, erdf) %>%
  fst::write_fst(file.path("data-raw","betas", "betas2_sd-2.fst"))


betas %>%
  dplyr::filter(
    sub > 10 &
      correlations == "none" &
      derivs == "none" ) %>%
  dplyr::mutate(
    flag2 = flag,
    session = sub%%10,
    sub = floor(sub/10),
    voxel = fct_cross(factor(voxel), factor(session)),
    voxel = as.character(voxel)) %>%
  dplyr::group_nest(sub, derivs, correlations, flag2) %>%
  dplyr::mutate(data = purrr::map(data, vtuner::pRF_filter, sdthresh=3)) %>%
  tidyr::unnest(cols=c(data)) %>%
  dplyr::filter(where == "middle") %>%
  rename(y = beta) %>%
  rename(ses = session) %>%
  select(sub, run2, voxel, contrast, orientation, y, ses, spmT, spmTavg, spmF, erdf) %>%
  fst::write_fst(file.path("data-raw","betas", "betas2-ses_sd-3.fst"))



