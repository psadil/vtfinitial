#' here, the thresholds are calculated _within_ subjects, so
#' that the resulting voxels always include multiple participants

library(tidyverse)
library(patchwork)

# low number of draws for the same of RAM
n_draw <- 200
base_size <- 8
base_family <- "ArialMT"

# cache <- drake::drake_cache("data-raw/caches/model-fit-everyone_one-sig")
cache <- drake::drake_cache("data-raw/caches/model-fit-everyone_sig-by-vox")
post_m <- drake::readd(post_multiplicative_vtf0_collapsed, cache=cache)


# --- plot voxel-wise VTFs with model fit

ss <- vtuner:::standata(post_m)

#' a bit hacky, but this reorders the raw data in the same way that
#' was done prior to model fitting. Now,
n_voxels <- 16
d0 <- vtuner::rawdata(post_m) %>%
  mutate(
    orientation = (CircStats::deg(orientation)+180)/2,
    orientation = round(orientation,digits = 2)) %>%
  dplyr::arrange(voxel,contrast,orientation) %>%
  mutate(x = ss$X) |>
  select(voxel, contrast, orientation, x)

d <- vtuner::rawdata(post_m) %>%
  select(sub, run2, voxel, contrast, orientation, y, ses) %>%
  mutate(
    orientation = (CircStats::deg(orientation)+180)/2,
    orientation = round(orientation,digits = 2)) %>%
  filter(!(fct_match(sub, "0") & ses == 1)) |>
  pivot_wider(names_from = contrast, values_from = y) |>
  group_by(voxel) |>
  mutate(avg = mean(high - low)) |>
  ungroup() |>
  group_nest(voxel, avg) |>
  slice_max(order_by = avg, n = n_voxels) |>
  unnest(data) |>
  pivot_longer(
    cols = c(low, high),
    names_to = "contrast",
    values_to = "y") |>
  whoppeR::WISEsummary(
    dependentvars = "y",
    withinvars = c("contrast", "orientation"),
    betweenvars = "voxel",
    idvar = "voxel") |>
  left_join(d0, by = c("voxel", "contrast", "orientation"))

# vtf_m <- vtuner:::.make_vtf0(post_m, cores=7)
# qs::qsave(vtf_m, "data-raw/vtf_m.qs")
vtf_m <- qs::qread("data-raw/vtf_m-sig-by-vox.qs")

vv_m0 <- posterior::as_draws_df(vtf_m) %>%
  as_tibble() %>%
  filter(.draw <= n_draw) %>%
  tidyr::pivot_longer(
    cols = c(-.iteration, -.chain, -.draw),
    names_to = "x",
    names_pattern = "...([[:digit:]]+)",
    values_to = ".estimate") %>%
  mutate(x = as.numeric(x))
rm(vtf_m)

m <- vv_m0 %>%
  semi_join(d, by = c("x")) %>%
  group_by(x) %>%
  summarise(
    lower = quantile(.estimate, 0.025),
    upper = quantile(.estimate, 0.975),
    multiplicative = mean(.estimate),
    .groups="drop") |>
  left_join(d, by = "x")


# --- plot distribution of slopes on full data ---
d <- vtuner::rawdata(post_m) %>%
  select(sub, run2, voxel, contrast, orientation, y, ses) %>%
  mutate(
    orientation = (CircStats::deg(orientation)+180)/2,
    orientation = round(orientation,digits = 2)) %>%
  mutate(
    orientation = factor(orientation),
    ses = factor(ses))

slopes <- d %>%
  pivot_wider(names_from = contrast, values_from = y) %>%
  group_nest(voxel, sub) %>%
  mutate(
    out = map(data, ~pracma::odregress(.x$low, .x$high)),
    slope = map_dbl(out, ~.x$coeff[[1]]),
    slope = atan2(slope, 1) |> CircStats::deg(),
    intercept = map_dbl(out, ~.x$coeff[[2]])) %>%
  select(slope, intercept, voxel, sub)

labels <- slopes %>%
  summarise(med = median(slope), .groups="drop") %>%
  mutate(text = glue::glue("  Median: {round(med, digits=3)}  "))

C <- slopes |>
  ggplot() +
  geom_histogram(aes(x=slope), bins=50, position = position_dodge()) +
  # facet_wrap(~sub) +
  geom_vline(xintercept=45) +
  # geom_vline(aes(xintercept=med), linetype = "dashed", data=labels) +
  # geom_text(aes(label=text, x=med), y=Inf, data=labels, hjust="outward", vjust=1.5) +
  ylab("Number of Voxels") +
  scale_x_continuous(
    name = "Orthogonal Regression Slope (deg)",
    limits = c(-90, 90),
    labels = c(-90, -45, 0, 45, 90),
    breaks = c(-90, -45, 0, 45, 90)) +
  theme_gray(base_family = base_family, base_size = base_size)


# ----- plot slope test comparison on observed data
find_rect <- function(d) {
  d |>
    group_by(voxel, orientation) |>
    mutate(low_avg = mean(low), high_avg = mean(high)) |>
    group_by(voxel) |>
    summarise(
      x0 = min(low_avg),
      x1 = max(low_avg),
      y0 = min(high_avg),
      y1 = max(high_avg),
      .groups = "drop")
}

find_segment <- function(d, voxels){
  d |>
    left_join(voxels, by = "voxel") |>
    group_nest(voxel, x0, x1, y0, y1) |>
    mutate(
      out = map(data, ~pracma::odregress(.x$low, .x$high)),
      slope = map_dbl(out, ~.x$coeff[[1]]),
      intercept = map_dbl(out, ~.x$coeff[[2]])) |>
    select(-out) |>
    rowwise() |>
    mutate(
      x = if_else(slope > 0, max(x0, (y0 - intercept)/slope), max(x0, (y1 - intercept)/slope)),
      xend = if_else(slope > 0, min(x1, (y1 - intercept)/slope), min(x1, (y0 - intercept)/slope))) |>
    ungroup() |>
    mutate(
      y = intercept + slope*x,
      yend = intercept + slope*xend) |>
    select(voxel, data, slope, intercept, x, xend, y, yend)
}

c0 <- vtuner::rawdata(post_m) %>%
  select(sub, run2, voxel, contrast, orientation, y, ses) %>%
  mutate(
    orientation = (CircStats::deg(orientation)+180)/2,
    orientation = round(orientation,digits = 2))|>
  pivot_wider(names_from = contrast, values_from = y)

voxels <- find_rect(c0)

cout <- find_segment(c0, voxels) |>
  mutate(model = "Orthogonal Regression")

avgs <- cout |>
  select(voxel, data) |>
  unnest(data) |>
  group_by(voxel, orientation) |>
  summarise(low=mean(low), high=mean(high), .groups = "drop")

c1 <- avgs |>
  ggplot() +
  coord_fixed() +
  geom_abline(slope=1, intercept=0) +
  geom_point(aes(x=low, y=high), alpha = 0.1) +
  geom_segment(
    aes(x=x,y=y,xend=xend,yend=yend, group=voxel),
    data = cout,
    alpha = 0.2) +
  scale_x_continuous(
    name = expression(paste("Observed Low Contrast ", beta))) +
  scale_y_continuous(
    name = expression(paste("Observed High Contrast ", beta))) +
  theme_classic(
    base_size = base_size,
    base_family = base_family) +
  theme(rect = element_blank())


# ---- next, plot show voxelwise slopes from model fits
#' a bit hacky, but this reorders the raw data in the same way that
#' was done prior to model fitting.
ss <- vtuner:::standata(post_m)
dd <- vtuner::rawdata(post_m) %>%
  dplyr::mutate(
    orientation_tested = as.numeric(factor(round(orientation,3)))) %>%
  dplyr::arrange(voxel,contrast,orientation) %>%
  mutate(x = ss$X)

# vtf_a <- vtuner:::.make_vtf0(post, cores=6)
# qs::qsave(vtf_a, "data-raw/vtf_a.qs")
vtf_a <- qs::qread("data-raw/vtf_a-sig-by-vox.qs")

vv_a0 <- posterior::as_draws_df(vtf_a) %>%
  as_tibble() %>%
  filter(.draw <= n_draw) %>%
  tidyr::pivot_longer(
    cols = c(-.iteration, -.chain, -.draw),
    names_to = "x",
    names_pattern = "...([[:digit:]]+)",
    values_to = ".estimate") %>%
  mutate(x = as.numeric(x))
rm(vtf_a)

a <- vv_a0 %>%
  left_join(dd, by = c("x")) %>%
  group_by(voxel, orientation, contrast) |>
  summarise(y = mean(.estimate),.groups = "drop") |>
  pivot_wider(names_from = contrast, values_from = y) |>
  find_segment(voxels) |>
  mutate(model = "Additive")


vtf_m <- qs::qread("data-raw/vtf_m-sig-by-vox.qs")

vv_m0 <- posterior::as_draws_df(vtf_m) %>%
  as_tibble() %>%
  filter(.draw <= n_draw) %>%
  tidyr::pivot_longer(
    cols = c(-.iteration, -.chain, -.draw),
    names_to = "x",
    names_pattern = "...([[:digit:]]+)",
    values_to = ".estimate") %>%
  mutate(x = as.numeric(x))
rm(vtf_m)

m <- vv_m0 %>%
  left_join(dd, by = c("x")) %>%
  group_by(voxel, orientation, contrast) |>
  summarise(y = mean(.estimate),.groups = "drop") |>
  pivot_wider(names_from = contrast, values_from = y) |>
  find_segment(voxels) |>
  mutate(model = "Multiplicative")

out <- bind_rows(a, m) |>
  bind_rows(cout) |>
  mutate(
    model = factor(
      model,
      levels = c("Orthogonal Regression", "Additive", "Multiplicative")))


B <- out %>%
  ggplot() +
  facet_wrap(~model, nrow = 1, labeller = labeller(.cols=label_wrap_gen(width=10))) +
  geom_abline(slope=1, intercept = 0) +
  geom_segment(
    aes(x=x,y=y,xend=xend,yend=yend),
    alpha = 0.3) +
  # coord_fixed() +
  coord_cartesian(xlim = c(-5, 25), ylim = c(-5, 30)) +
  scale_x_continuous(
    name = expression(paste("Low Contrast ", beta))) +
  scale_y_continuous(
    name = expression(paste("High Contrast ", beta))) +
  theme_classic(
    base_size = base_size,
    base_family = base_family) +
  theme(rect = element_blank())

p <- B + C +
  plot_annotation(tag_levels = "a")


ggsave(
  here::here("figures", "figure-5.png"),
  plot = p,
  width = 5,
  height = 2.5,
  dpi=600,
  device = ragg::agg_png())
dev.off()

# ragg::agg_png(
#   filename = here::here("figures", "figure-5.png"),
#   width = 5,
#   height = 5,
#   res = 600,
#   units = "in")
# p
# dev.off()

