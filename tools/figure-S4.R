library(tidyverse)
library(cmdstanr)

cache <- drake::drake_cache("data-raw/caches/model-fit-everyone_one-sig")
post_m <- drake::readd(post_multiplicative_vtf0_collapsed, cache=cache)

ss <- vtuner:::standata(post_m)
d <- vtuner::rawdata(post_m) %>%
  select(sub, run2, voxel, contrast, orientation, y, ses) %>%
  mutate(
    # voxel = as.numeric(voxel),
    orientation = (CircStats::deg(orientation)+180)/2,
    orientation = round(orientation,digits = 2)) %>%
  # filter(!(fct_match(sub, "0")))
  filter(!(fct_match(sub, "0") & ses == 1)) %>%
  mutate(
    orientation = factor(orientation),
    ses = factor(ses))


ranks_vox <- d %>%
  group_by(voxel, contrast, sub) %>%
  summarise(y = mean(y), .groups = "drop") %>%
  pivot_wider(names_from = contrast, values_from = y) %>%
  mutate(di = high - low) %>%
  crossing(Threshold = c(0.9)) %>%
  group_by(Threshold, sub) %>%
  filter(di >= quantile(di, Threshold)) %>%
  select(-di, -low, -high) %>%
  ungroup()

data_list <- d %>%
  # inner_join(ranks_vox, by = c("voxel", "sub")) %>%
  # mutate(voxel = fct_drop(voxel)) %>%
  pivot_wider(names_from = contrast, values_from = y) %>%
  arrange(sub, voxel, orientation) %>%
  rename(x=low, y=high) %>%
  mutate(voxel_orientation = interaction(voxel, orientation, lex.order = TRUE)) %>%
  tidybayes::compose_data() %>%
  vctrs::vec_c(list(centered_mu = 1))

m <- cmdstan_model(
  here::here('tools', "errors_in_variables02-wsigmazxy_centxysig.stan"),
  dir = "tools",
  pedantic = TRUE)

fit <- m$sample(
  data = data_list,
  chains = 5,
  parallel_chains = 5)

# stanfit <- rstan::read_stan_csv(fit$output_files())
# shinystan::launch_shinystan(stanfit)


fit <- readRDS("data-raw/sigmazxy.RDS")

params <- c("alpha_mu", "alpha_sigma",
            "beta_mu", "beta_sigma",
            "sigma_x_mu", "sigma_x_sigma",
            "sigma_y_mu", "sigma_y_sigma",
            "sigma_z_mu", "sigma_z_sigma",
            "sigma_b", "mu_mu")

fit$summary(params)

params2 <- c("beta_mu", "beta_sigma",
             "sigma_x_mu", "sigma_x_sigma",
             "sigma_y_mu", "sigma_y_sigma",
             "sigma_z_mu", "sigma_z_sigma")

bayesplot::mcmc_pairs(fit$draws(params2))

params3 <- c("beta_mu", "sigma_x_mu","sigma_y_mu")

draws <- fit$draws(params3) %>%
  posterior::as_draws_df()


palatte <- viridisLite::inferno(3, end=0.8)

library(patchwork)

a <- draws %>%
  as_tibble() %>%
  select(-beta_mu) %>%
  pivot_longer(cols = contains("mu"), names_to = "Contrast", values_to = "Noise") %>%
  mutate(
    Contrast = factor(Contrast, levels = c("sigma_x_mu", "sigma_y_mu")),
    Contrast = fct_recode(
      Contrast,
      `Low` = "sigma_x_mu",
      `High` = "sigma_y_mu")) %>%
  ggplot(aes(x=Noise, fill=Contrast)) +
  geom_histogram(bins=50, position = position_dodge()) +
  scale_fill_manual(
    values = palatte[2:3],
    labels = scales::parse_format()) +
  ylab("Count") +
  theme_gray(
    base_family = "ArialMT",
    base_size = 9) +
  theme(legend.position = "bottom")

b <- draws %>%
  as_tibble() %>%
  select(-contains("sigma"), Slope="beta_mu") %>%
  ggplot(aes(x=Slope)) +
  geom_histogram(bins=50) +
  ylab("Count") +
  scale_x_continuous(
    breaks = c(1.3, 1.35, 1.4, 1.45),
    labels = c(1.3, 1.35, 1.4, 1.45)) +
  theme_gray(
    base_family = "ArialMT",
    base_size = 9)

a + b +
  plot_layout(nrow=1) +
  plot_annotation(tag_levels = "a") +
  ggsave(
    here::here("figures", "figure-S4.png"),
    device = ragg::agg_png(),
    height = 3,
    width = 5)
