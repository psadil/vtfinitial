#!/bin/bash

#BSUB -J primary-i51-100                      # name of the job / array jobs
#BSUB -o /home/ps52a/logs/primary-i51-100.txt # stdout + stderr
#BSUB -R rusage[mem=20480]            # Memory requirements in Mbytes
#BSUB -q long                        # name of the queue
#BSUB -W 145:00                       # walltime
#BSUB -n 1
#BSUB -R "span[hosts=1]"

module load zeromq/4.1.4
module load R/3.6.1

Rscript --no-save --no-restore --quiet -e "drake::r_make('tools/wf_informed51-100.R', r_args = list(user_profile=TRUE, system_profile=TRUE))"
