#!/bin/bash

#BSUB -J primary-d0                      # name of the job / array jobs
#BSUB -o /home/ps52a/logs/primary-d0.txt # stdout + stderr
#BSUB -R rusage[mem=2048]            # Memory requirements in Mbytes
#BSUB -q long                        # name of the queue
#BSUB -W 73:00                       # walltime
#BSUB -n 1
#BSUB -R "span[hosts=1]"

module load zeromq/4.1.4
module load R/3.6.1

Rscript --no-save --no-restore --quiet tools/wf_fit-subs.R
