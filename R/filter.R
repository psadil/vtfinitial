fftshift <- function(input_matrix, dim = -1) {

  rows <- dim(input_matrix)[1]
  cols <- dim(input_matrix)[2]

  swap_up_down <- function(input_matrix) {
    rows_half <- ceiling(rows/2)
    return(rbind(input_matrix[((rows_half+1):rows), (1:cols)], input_matrix[(1:rows_half), (1:cols)]))
  }

  swap_left_right <- function(input_matrix) {
    cols_half <- ceiling(cols/2)
    return(cbind(input_matrix[1:rows, ((cols_half+1):cols)], input_matrix[1:rows, 1:cols_half]))
  }

  if (dim == -1) {
    input_matrix <- swap_up_down(input_matrix)
    return(swap_left_right(input_matrix))
  }
  else if (dim == 1) {
    return(swap_up_down(input_matrix))
  }
  else if (dim == 2) {
    return(swap_left_right(input_matrix))
  }
  else {
    stop("Invalid dimension parameter")
  }
}

ifftshift <- function(input_matrix, dim = -1) {

  rows <- dim(input_matrix)[1]
  cols <- dim(input_matrix)[2]

  swap_up_down <- function(input_matrix) {
    rows_half <- floor(rows/2)
    return(rbind(input_matrix[((rows_half+1):rows), (1:cols)], input_matrix[(1:rows_half), (1:cols)]))
  }

  swap_left_right <- function(input_matrix) {
    cols_half <- floor(cols/2)
    return(cbind(input_matrix[1:rows, ((cols_half+1):cols)], input_matrix[1:rows, 1:cols_half]))
  }

  if (dim == -1) {
    input_matrix <- swap_left_right(input_matrix)
    return(swap_up_down(input_matrix))
  }
  else if (dim == 1) {
    return(swap_up_down(input_matrix))
  }
  else if (dim == 2) {
    return(swap_left_right(input_matrix))
  }
  else {
    stop("Invalid dimension parameter")
  }
}



sig <- function(x, alpha){
  1/(1+exp(-alpha*x))
}

isotropic <- function(s, qc=0.0775, delta=0.15, alpha){
  s1 <- (qc - delta/2)
  s2 <- (qc + delta/2)
  sig(s1 - s, alpha) * sig(s - s2, alpha)
}

fn <- function(pars,
               n = 256,
               voxsize = 0.225,
               n_vox = 20,
               seed = 1234) {

  set.seed(seed)
  x <- matrix(rnorm(n^2), nrow = n, ncol = n)

  H <- crossing(
    x = seq(-pi, pi, length.out = n),
    y = seq(-pi, pi, length.out = n),
    alpha = pars[1],
    Qc = c(0.0775)*pi,
    delta = pars[2]) %>%
    mutate(
      z = isotropic(sqrt(x^2+y^2), Qc, delta, alpha=alpha)) %>%
    group_nest(alpha, Qc, delta) %>%
    mutate(data = map(data, ~.x %>%
                        pivot_wider(names_from = x, values_from = z) %>%
                        select(-y) %>%
                        as.matrix()),
           h = map(data, imager::as.cimg),
           X = map(data, ~ifftshift(Re(fft(ifftshift(fftshift(fft(x)) * .x), inverse=TRUE))/n^2) %>%
                     magrittr::set_colnames(1:n) %>%
                     magrittr::set_rownames(1:n)),
           gr = map(X, ~imager::imgradient(imager::as.cimg(.x)) %>%
                      magrittr::set_names( c("dx","dy")) %>%
                      as.data.frame() %>%
                      tidyr::spread(im, value) %>%
                      as_tibble() %>%
                      mutate(ori = CircStats::deg((atan2(dy, dx)+pi)/2)))) %>%
    select(-data, -X, -h) %>%
    mutate(
      alpha = factor(alpha),
      Qc = factor(Qc, labels = round(unique(Qc), digits=3)),
      delta = factor(delta, labels = round(unique(delta), digits = 3))) %>%
    unnest(cols = gr) %>%
    mutate(
      x = (x / max(x) - 0.5)*2,
      y = (y / max(y) - 0.5)*2 ) %>%
    filter(sqrt(x^2 + y^2) <= 1)

  positions <- H %>%
    distinct(x, y) %>%
    filter(sqrt(x^2 + y^2) <= 3/4) %>%
    sample_n(n_vox) %>%
    mutate(vox = 1:n()) %>%
    rename(x0 = x, y0=y) %>%
    crossing(voxsize = voxsize)

  tmp <-  H %>%
    crossing(positions) %>%
    mutate(
      xx = abs(x-x0),
      yy = abs(y-y0)) %>%
    filter(sqrt((x-x0)^2 + (y-y0)^2) <= voxsize)

  out <- tmp %>%
    group_nest(vox) %>%
    mutate(test = map_dbl(data, ~diptest::dip(.x$ori, min.is.0 = TRUE)))

  return(sum(out$test))
}
